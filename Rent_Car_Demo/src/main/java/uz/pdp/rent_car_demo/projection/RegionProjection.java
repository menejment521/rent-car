package uz.pdp.rent_car_demo.projection;

import org.springframework.data.rest.core.config.Projection;
import uz.pdp.rent_car_demo.entity.Region;

import java.util.UUID;

@Projection(types = Region.class)
public interface RegionProjection {
    UUID getId();
    String getName();
}
