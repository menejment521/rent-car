package uz.pdp.rent_car_demo.projection;

import org.springframework.data.rest.core.config.Projection;
import uz.pdp.rent_car_demo.entity.PriceType;

import java.util.UUID;

@Projection(types = PriceType.class)
public interface PriceProjection {

    String getName();

    UUID getId();
}
