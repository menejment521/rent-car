package uz.pdp.rent_car_demo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.pdp.rent_car_demo.entity.enums.StatusName;
import uz.pdp.rent_car_demo.entity.template.AbsEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class StatusOrder extends AbsEntity {

    @Column(nullable = false, unique = true)
    @Enumerated(value = EnumType.STRING)
    private StatusName statusName;
}
