package uz.pdp.rent_car_demo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.pdp.rent_car_demo.entity.template.AbsEntity;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class PriceOfShablon extends AbsEntity {

    @OneToOne(fetch = FetchType.LAZY)
    private Shablon shablon;

    @ManyToOne(fetch = FetchType.LAZY)
    private PriceType priceType;

    @Column(nullable = false)
    private double price;

    @Column(nullable = true)
    private double extraPrice;
}
