package uz.pdp.rent_car_demo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.pdp.rent_car_demo.entity.template.AbsEntity;

import javax.persistence.*;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class RealCar extends AbsEntity {

    @ManyToOne(fetch = FetchType.LAZY)
    private Shablon shablon;

    @Column(nullable = false, unique = true)
    private String carNumber;

    private Double importPrice;

    private Integer year;

    private boolean onWay;

    private boolean block=true;

}
