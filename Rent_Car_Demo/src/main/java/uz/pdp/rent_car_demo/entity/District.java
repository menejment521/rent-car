package uz.pdp.rent_car_demo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.pdp.rent_car_demo.entity.template.AbsEntityName;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class District extends AbsEntityName {

    @ManyToOne(fetch = FetchType.LAZY)
    private Region region;
}
