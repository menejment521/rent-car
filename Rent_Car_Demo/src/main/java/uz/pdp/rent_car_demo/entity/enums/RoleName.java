package uz.pdp.rent_car_demo.entity.enums;

public enum RoleName {
    OWNER,
    MANAGER,
    ADMIN,
    ADMINSRATOR,
    TECHNICAL_STAFF,
    USER
}
