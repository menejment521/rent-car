package uz.pdp.rent_car_demo.entity.enums;

public enum StatusName {
    FAVOURITE,
    BASKET,
    NEW,
    ON_WAY,
    GIVE_BACK
}
