package uz.pdp.rent_car_demo.entity.template;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@Data
@AllArgsConstructor
@NoArgsConstructor
@MappedSuperclass
public class AbsEntityName extends AbsEntity{

    @Column(nullable = false, unique = true)
    private String name;
}
