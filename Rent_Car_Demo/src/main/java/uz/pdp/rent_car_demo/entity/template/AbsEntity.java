package uz.pdp.rent_car_demo.entity.template;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public class AbsEntity {

    @Id
    @GeneratedValue
    private UUID id;

    @CreatedBy
    @Column(updatable = false)
    private UUID createdBy;

    @CreationTimestamp
    @Column(updatable = true)
    private Timestamp createdAt;

    @LastModifiedBy
    private UUID updatedBy;

    @UpdateTimestamp
    private Timestamp updatedAt;
}
