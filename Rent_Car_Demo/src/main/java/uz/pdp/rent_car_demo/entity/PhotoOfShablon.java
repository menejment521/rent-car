package uz.pdp.rent_car_demo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.pdp.rent_car_demo.entity.template.AbsEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class PhotoOfShablon extends AbsEntity {

    @ManyToOne(fetch = FetchType.LAZY)
    private Shablon shablonPhoto;

    @Column(nullable = false, unique = false)
    private String photoUrl;
}
