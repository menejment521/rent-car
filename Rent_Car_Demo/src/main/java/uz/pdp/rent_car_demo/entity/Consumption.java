package uz.pdp.rent_car_demo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.pdp.rent_car_demo.entity.template.AbsEntity;

import javax.persistence.Entity;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Consumption extends AbsEntity {

    private Double fuelConsumption;

    private String measurementFuel;

    private Double distance;

    private String measurementDistance;
}
