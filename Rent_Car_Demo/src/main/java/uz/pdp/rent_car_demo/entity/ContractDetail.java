package uz.pdp.rent_car_demo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.pdp.rent_car_demo.entity.template.AbsEntity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class ContractDetail extends AbsEntity {

    private String detailName;

    private Double minPrice;

    private Double maxPrice;

    @ManyToOne(fetch = FetchType.LAZY)
    private Shablon shablon;
}
