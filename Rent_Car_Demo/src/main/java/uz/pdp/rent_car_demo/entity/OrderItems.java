package uz.pdp.rent_car_demo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.pdp.rent_car_demo.entity.template.AbsEntity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class OrderItems extends AbsEntity {

    @ManyToOne(fetch = FetchType.LAZY)
    private Shablon shablon;

    @ManyToMany
    private Set<RealCar> realCars;

    @ManyToOne(fetch = FetchType.LAZY)
    private Order order;

    @Column(nullable = false)
    private int amountCar;

    @ManyToOne(fetch = FetchType.LAZY)
    private PriceType priceType;

    @Column(nullable = false)
    private int rentCount;

    @Column(nullable = false)
    private Timestamp sendTime;

    @Column(nullable = false)
    private Double priceOneCar;
}
