package uz.pdp.rent_car_demo.service;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import uz.pdp.rent_car_demo.entity.Role;
import uz.pdp.rent_car_demo.entity.User;
import uz.pdp.rent_car_demo.entity.enums.RoleName;
import uz.pdp.rent_car_demo.payload.ApiResponse;
import uz.pdp.rent_car_demo.payload.RegisterDto;
import uz.pdp.rent_car_demo.payload.ResToken;
import uz.pdp.rent_car_demo.payload.SignInDto;
import uz.pdp.rent_car_demo.repo.RoleRepo;
import uz.pdp.rent_car_demo.repo.UserRepo;
import uz.pdp.rent_car_demo.secret.MyJwtProvider;

import java.util.HashSet;
import java.util.Set;

@Service
public class UserService {

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private MyJwtProvider provider;

    @Autowired
    private RoleRepo roleRepo;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public ResToken login(SignInDto signInDto) {
        Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
                signInDto.getPhoneNumber(),
                signInDto.getPassword()
        ));
        User user = (User) authentication.getPrincipal();
        String token = provider.generateToken(user);
//        User byPhoneNumber = userRepo.findByPhoneNumber(signInDto.getPhoneNumber());
//        String s = provider.generateToken(byPhoneNumber);
        return new ResToken(token);
    }

    public ApiResponse register(RegisterDto registerDto) {
        boolean res = userRepo.existsByPhoneNumber(registerDto.getPhoneNumber());
        if(!res) {
            Set<Role> roleSet = new HashSet<>();
            roleSet.add(roleRepo.findByRoleName(RoleName.USER));
            userRepo.save(
                    new User(registerDto.getFirstName(), registerDto.getLastName(),
                            registerDto.getPhoneNumber(), registerDto.getEmail(),
                            registerDto.getAge(), registerDto.getPassportSeria(),
                            roleSet, passwordEncoder.encode(registerDto.getPassword()), 0, 0));
            return new ApiResponse(true,"Saved");
        }
        return new ApiResponse(false, "User exist");
    }


}
