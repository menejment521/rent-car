package uz.pdp.rent_car_demo.payload;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.pdp.rent_car_demo.entity.Role;

import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RegisterDto {
    private String firstName;
    private String lastName;
    private String phoneNumber;
    private String email;
    private String passportSeria;
    private int age;
    private String password;




}
