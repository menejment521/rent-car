package uz.pdp.rent_car_demo.controller;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import uz.pdp.rent_car_demo.entity.Role;
import uz.pdp.rent_car_demo.entity.User;
import uz.pdp.rent_car_demo.entity.enums.RoleName;
import uz.pdp.rent_car_demo.payload.ApiResponse;
import uz.pdp.rent_car_demo.payload.RegisterDto;
import uz.pdp.rent_car_demo.payload.ResToken;
import uz.pdp.rent_car_demo.payload.SignInDto;
import uz.pdp.rent_car_demo.repo.RoleRepo;
import uz.pdp.rent_car_demo.repo.UserRepo;
import uz.pdp.rent_car_demo.service.UserService;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@RestController
@RequestMapping("/api/auth")
//@CrossOrigin(origins = {"Bu yerga klintni porti yoziladi"})
public class AuthController {

    @Autowired
     UserRepo userRepo;

     @Autowired
     UserService userService;

     @Autowired
     PasswordEncoder passwordEncoder;

     @Autowired
     RoleRepo roleRepo;

    @PostMapping("/login")
    public HttpEntity<?> login(@RequestBody SignInDto signInDto){
        ResToken resToken = userService.login(signInDto);
        return ResponseEntity.ok(resToken);
    }

    @PostMapping("/register")
    public HttpEntity<?> register(@RequestBody RegisterDto registerDto){
        ApiResponse apiResponse = userService.register(registerDto);
        return ResponseEntity.status(apiResponse.getMessage().equals("Saved")?
                201 : 409).body(apiResponse);
    }
}
