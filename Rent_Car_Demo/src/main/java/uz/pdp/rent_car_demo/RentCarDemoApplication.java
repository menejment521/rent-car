package uz.pdp.rent_car_demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RentCarDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(RentCarDemoApplication.class, args);
    }

}
