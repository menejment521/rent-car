package uz.pdp.rent_car_demo.components;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import uz.pdp.rent_car_demo.entity.Role;
import uz.pdp.rent_car_demo.entity.User;
import uz.pdp.rent_car_demo.entity.enums.RoleName;
import uz.pdp.rent_car_demo.repo.RoleRepo;
import uz.pdp.rent_car_demo.repo.UserRepo;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

@Component
public class DataLoader implements CommandLineRunner {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private RoleRepo roleRepo;

    @Autowired
    private UserRepo userRepo;

    @Override
    public void run(String... args) throws Exception {
        Role owner=new Role(RoleName.OWNER);
        Role admin=new Role(RoleName.ADMIN);
        Role manager=new Role(RoleName.MANAGER);
        Role adminsrator=new Role(RoleName.ADMINSRATOR);
        Role technicalStaff=new Role(RoleName.TECHNICAL_STAFF);
        Role user=new Role(RoleName.USER);
        roleRepo.save(owner);
        roleRepo.save(admin);
        roleRepo.save(adminsrator);
        roleRepo.save(manager);
        roleRepo.save(technicalStaff);
        roleRepo.save(user);

        Set<Role> roleSet=new HashSet<>();
        roleSet.add(owner);
        roleSet.add(admin);
        roleSet.add(adminsrator);
        roleSet.add(technicalStaff);
        roleSet.add(user);
        User own=new User("Owner", "Director", "+998901234567", "owner@gmail.com", 25, "AB1234567", roleSet, passwordEncoder.encode("123"), 0, 0.0);
        userRepo.save(own);

        Set<Role> roleSet1=new HashSet<>();
        roleSet.add(user);
        User user1=new User("User", "Simple User", "+998976543210", "user@gmail.com", 20, "AB7654321", roleSet1, passwordEncoder.encode("123"), 0, 0.0);
        userRepo.save(user1);
    }
}
