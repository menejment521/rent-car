package uz.pdp.rent_car_demo.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.pdp.rent_car_demo.entity.PriceType;
import uz.pdp.rent_car_demo.projection.PriceProjection;

@RepositoryRestResource(path = "pricetype",collectionResourceRel = "list",excerptProjection = PriceProjection.class)
public interface PriceTypeRepo extends JpaRepository<PriceType, Long> {

  //  boolean existsByName(String name);

}
