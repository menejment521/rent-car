package uz.pdp.rent_car_demo.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.security.access.prepost.PreAuthorize;
import uz.pdp.rent_car_demo.entity.Region;
import uz.pdp.rent_car_demo.projection.RegionProjection;

import java.util.List;
import java.util.UUID;

@RepositoryRestResource(path = "region", collectionResourceRel = "list", excerptProjection = RegionProjection.class)
public interface RegionRepo extends JpaRepository<Region, UUID> {


    @PreAuthorize("hasAnyRole('ADMIN')")
    @Override
    List<Region> findAll();

}
