package uz.pdp.rent_car_demo.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.rent_car_demo.entity.Role;
import uz.pdp.rent_car_demo.entity.enums.RoleName;

import java.util.UUID;


public interface RoleRepo extends JpaRepository<Role, UUID> {
    Role findByRoleName(RoleName roleName);

}
