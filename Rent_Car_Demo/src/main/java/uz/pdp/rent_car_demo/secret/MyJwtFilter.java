package uz.pdp.rent_car_demo.secret;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;
import uz.pdp.rent_car_demo.entity.User;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class MyJwtFilter  extends OncePerRequestFilter {

    @Autowired
    private  MyJwtProvider myJwtProvider;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        String tokenFromRequest = getTokenFromRequest(request);
        if (tokenFromRequest != null) {
            User user = getUserFromToken(tokenFromRequest);
            if (user != null) {
                if (user.isAccountNonExpired()) {
                    if (user.isAccountNonLocked()) {
                        if (user.isCredentialsNonExpired()) {
                            if (user.isEnabled()) {
                                UsernamePasswordAuthenticationToken authenticationToken =
                                        new UsernamePasswordAuthenticationToken(
                                                user,
                                                null,
                                                user.getAuthorities()
                                        );
                                SecurityContextHolder.getContext().setAuthentication(authenticationToken);
                            } else {
                                System.err.println("User Disabled");
                            }
                        } else {
                            System.err.println("User Credentials Expired");
                        }
                    } else {
                        System.err.println("User Locked");
                    }
                } else {
                    System.err.println("User Expired");
                }
            }
        }
        filterChain.doFilter(request, response);
    }

    public String getTokenFromRequest(HttpServletRequest request) {
        String header = request.getHeader("Authorization");
        return header != null ? header.substring(7) : null;
    }

    public User getUserFromToken(String token) {
        boolean validateToken = myJwtProvider.validateToken(token);
        if (validateToken) {
            return myJwtProvider.getUserFromToken(token);
        }
        return null;

    }
}
