package uz.pdp.rent_car_demo.secret;


import io.jsonwebtoken.*;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import uz.pdp.rent_car_demo.entity.User;
import uz.pdp.rent_car_demo.repo.UserRepo;

import java.util.Date;
import java.util.Optional;
import java.util.UUID;

@Component
public class MyJwtProvider {

    @Value("${jwt.secretKey}")
    private String secretKey;

    @Value("${jwt.expireDateInMilliSecund}")
    private Long expireDateInMilli;

    @Autowired
    private UserRepo userRepo;


    public String generateToken(User user) {
        Date date = new Date();
        long tokenExpireTime = date.getTime() + expireDateInMilli;
        Date expireDate = new Date(tokenExpireTime);

        return Jwts
                .builder()
                .setSubject(user.getId().toString())
                .setIssuedAt(date)
                .claim("roles", user.getRoles())
                .setExpiration(expireDate)
                .signWith(SignatureAlgorithm.HS512, secretKey)
                .compact();
    }

    public boolean validateToken(String token){
        try {
            Jwts.parser()
                    .setSigningKey(secretKey)
                    .parseClaimsJws(token);
            return true;
        }catch (ExpiredJwtException e) {
            System.err.println("Muddati o'tgan");
            Date expiration = Jwts.parser()
                    .setSigningKey(secretKey)
                    .parseClaimsJws(token)
                    .getBody()
                    .getExpiration();

        } catch (MalformedJwtException malformedJwtException) {
            System.err.println("Buzilgan token");
        } catch (SignatureException s) {
            System.err.println("Kalit so'z xato");
        } catch (UnsupportedJwtException unsupportedJwtException) {
            System.err.println("Qo'llanilmagan token");
        } catch (IllegalArgumentException ex) {
            System.err.println("Bo'sh token");
        }
        return false;

    }

    public User getUserFromToken(String token){
        String userId=Jwts
                .parser()
                .setSigningKey(secretKey)
                .parseClaimsJws(token)
                .getBody()
                .getSubject();
        System.out.println(userId);
        Optional<User> optionalUser = userRepo.findById(UUID.fromString(userId));
        return optionalUser.orElse(null);
//        return null;
    }
}
